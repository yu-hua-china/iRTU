# iRTU 开源DTU/RTU解决方案

基于合宙air202/208/800/801/720/724/H/D/G/U 系列模块的开源DTU/RTU解决方案

已成功运行在上百万设备, 安全可靠, 实力担当!

# iRTU 全名 LuatOS-iRTU
## 新增功能
新增加功能仅支持 SmartDTU V2.0 固件 请红色标注
- 1.增加： 串口参数 --> 485有效电平, 默认null
- 2.增加： 串口参数 --> 帧按格式打包: 按长度为number，按格式符打包("*l","*s","*n","*L","*p"),按时间打包 null,  默认null
- 3.增加： 串口参数 --> 帧按时间打包: 启用超时为number(ms)，流模式为0，不启用为null, 默认 10ms
- 4.增加： 串口参数 --> 主动采集任务定时间隔 默认 null 单位秒
- 5.增加： 串口参数 --> 采集指令数组【指令间隔ms, 指令1，指令2，指令3, ...】
- 6.增加： 串口参数 --> 被动采集任务定时间隔 默认 nul 单位l秒
- 7.增加： 串口参数 --> 被动采集任务采集时长 默认 nul 单位l秒
- 8.增加： 串口参数 --> 串口上行流量监控 number 默认 nul  每分钟流量
- 9.增加： 网络通道 -->所有通道下的所有子通道   -->  串口id，增加 usb.com 虚拟串口通道
- 10.增加：基本参数--> 无网络超时重启，  默认值 300，单位秒  字段为rstTim
- 11.增加： 基本参数--> 无网络超时进出飞行模式 默认 180，单位秒 字段为 flyTim
- 12.标注：基本参数--串口分帧超时 红字 --> 仅 iRTU V1.x.x 固件有效
- 13.标注：基本参数--网络分帧超时 红字 --> 仅 iRTU V1.x.x 固件有效
- 14.标注：基本参数--每分钟最大串口流量 --> 仅 iRTU V1.x.x 固件有效
- 15. 标注： 所有通道Socket --> 被动上报间隔  --> 仅 iRTU V1.x.x 固件有效
- 16. 标注： 所有通道Socket --> 被动采集间隔  --> 仅 iRTU V1.x.x 固件有效
- 17. 标注： 所有通道Socket --> 自动自动任务间隔  --> 仅 iRTU V1.x.x 固件有效
- 18. 增加： 所有通道Socket --> 登陆注册包 默认null
- 19. 增加： 所有通道Socket --> 断线重连间隔 默认5 秒
- 20. 增加： 所有通道Socket -->  帧按格式打包: 按长度为number，按格式符打包("*l","*s","*n","*L","*p"),按时间打包 null, 默认 null,
- 21. 增加： 所有通道Socket --> 帧按时间打包: 启用超时为number(ms)，流模式为0，不启用为null, 默认 null
- 22. 增加： 所有通道MQTT --> 登陆注册包 默认null
- 22. 增加： 所有通道MQTT --> 断线重连间隔 默认5 秒

## 主要功能

1. 支持TCP/UDP socket,支持HTTP,MQTT,等常见透传和非透传模式
2. 支持OneNET,阿里云，百度云，腾讯云等常见公有云。
3. 支持RTU主控模式
4. 支持数据流模版
5. 支持消息推送(电话，短信，网络通知)
6. 支持GPS数据以及相关数据采集
7. 支持ADC,I2C等外设，可以方便的扩展为屏幕、二维码等解决方案.
8. 需要将配置文件烧录到固件的，修改源码irtu.cfg文件，然后打包源码+lib+core 成固件即可; irtu.cfg 内包含demo，可以用web导出的配置json文件替换''(单引号内的json字符串)即可。

## 相关码云库

1. 合宙Air724U模块, 4G cat.1 https://gitee.com/openLuat/Luat_Lua_Air724U
2. 合宙Air720S模块, 4G cat.4 https://gitee.com/openLuat/Luat_CSDK_Air720S


## Wiki 和 Doc 网站

* http://wiki.openluat.com/
* http://doc.openluat.com/

## 交流群

* QQ群 1027923658

## 视频教程

* iRTU快速接入教程 https://www.bilibili.com/video/av41012302
* iRTU远程浇花视频 https://www.bilibili.com/video/av47478475
* Luat相关工具教程 https://www.bilibili.com/video/av50453083
* Luat硬件设计参考 https://www.bilibili.com/video/av45341487
* Luat开发视频教程 https://www.bilibili.com/video/av50827315

合宙官网 http://www.openluat.com
iRTU配置网页 http://dtu.openluat.com

## 授权协议

[MIT License](LICENSE)
